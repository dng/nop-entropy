//__XGEN_FORCE_OVERRIDE__
package io.nop.xlang.ast;

public enum XLangASTKind{

            Program, // ordinal: 0
        
            Identifier, // ordinal: 1
        
            Literal, // ordinal: 2
        
            TemplateStringLiteral, // ordinal: 3
        
            RegExpLiteral, // ordinal: 4
        
            BlockStatement, // ordinal: 5
        
            EmptyStatement, // ordinal: 6
        
            ReturnStatement, // ordinal: 7
        
            BreakStatement, // ordinal: 8
        
            ContinueStatement, // ordinal: 9
        
            IfStatement, // ordinal: 10
        
            SwitchStatement, // ordinal: 11
        
            SwitchCase, // ordinal: 12
        
            ThrowStatement, // ordinal: 13
        
            TryStatement, // ordinal: 14
        
            CatchClause, // ordinal: 15
        
            WhileStatement, // ordinal: 16
        
            DoWhileStatement, // ordinal: 17
        
            VariableDeclarator, // ordinal: 18
        
            VariableDeclaration, // ordinal: 19
        
            ForStatement, // ordinal: 20
        
            ForOfStatement, // ordinal: 21
        
            ForRangeStatement, // ordinal: 22
        
            ForInStatement, // ordinal: 23
        
            DeleteStatement, // ordinal: 24
        
            ChainExpression, // ordinal: 25
        
            ThisExpression, // ordinal: 26
        
            SuperExpression, // ordinal: 27
        
            TemplateStringExpression, // ordinal: 28
        
            ArrayExpression, // ordinal: 29
        
            ObjectExpression, // ordinal: 30
        
            PropertyAssignment, // ordinal: 31
        
            ParameterDeclaration, // ordinal: 32
        
            FunctionDeclaration, // ordinal: 33
        
            ArrowFunctionExpression, // ordinal: 34
        
            UnaryExpression, // ordinal: 35
        
            UpdateExpression, // ordinal: 36
        
            BinaryExpression, // ordinal: 37
        
            InExpression, // ordinal: 38
        
            ExpressionStatement, // ordinal: 39
        
            AssignmentExpression, // ordinal: 40
        
            LogicalExpression, // ordinal: 41
        
            MemberExpression, // ordinal: 42
        
            EvalExpression, // ordinal: 43
        
            CallExpression, // ordinal: 44
        
            NewExpression, // ordinal: 45
        
            SpreadElement, // ordinal: 46
        
            SequenceExpression, // ordinal: 47
        
            ConcatExpression, // ordinal: 48
        
            BraceExpression, // ordinal: 49
        
            ObjectBinding, // ordinal: 50
        
            PropertyBinding, // ordinal: 51
        
            RestBinding, // ordinal: 52
        
            ArrayBinding, // ordinal: 53
        
            ArrayElementBinding, // ordinal: 54
        
            ExportDeclaration, // ordinal: 55
        
            ExportNamedDeclaration, // ordinal: 56
        
            ExportAllDeclaration, // ordinal: 57
        
            ExportSpecifier, // ordinal: 58
        
            ImportDeclaration, // ordinal: 59
        
            ImportAsDeclaration, // ordinal: 60
        
            ImportSpecifier, // ordinal: 61
        
            ImportDefaultSpecifier, // ordinal: 62
        
            ImportNamespaceSpecifier, // ordinal: 63
        
            AwaitExpression, // ordinal: 64
        
            Decorators, // ordinal: 65
        
            QualifiedName, // ordinal: 66
        
            Decorator, // ordinal: 67
        
            MetaObject, // ordinal: 68
        
            MetaProperty, // ordinal: 69
        
            MetaArray, // ordinal: 70
        
            UsingStatement, // ordinal: 71
        
            MacroExpression, // ordinal: 72
        
            TextOutputExpression, // ordinal: 73
        
            EscapeOutputExpression, // ordinal: 74
        
            CollectOutputExpression, // ordinal: 75
        
            CompareOpExpression, // ordinal: 76
        
            AssertOpExpression, // ordinal: 77
        
            BetweenOpExpression, // ordinal: 78
        
            GenNodeExpression, // ordinal: 79
        
            GenNodeAttrExpression, // ordinal: 80
        
            OutputXmlAttrExpression, // ordinal: 81
        
            OutputXmlExtAttrsExpression, // ordinal: 82
        
            SwitchExpression, // ordinal: 83
        
            SwitchCaseExpression, // ordinal: 84
        
            TypeOfExpression, // ordinal: 85
        
            InstanceOfExpression, // ordinal: 86
        
            CastExpression, // ordinal: 87
        
            ArrayTypeNode, // ordinal: 88
        
            ParameterizedTypeNode, // ordinal: 89
        
            TypeNameNode, // ordinal: 90
        
            UnionTypeDef, // ordinal: 91
        
            IntersectionTypeDef, // ordinal: 92
        
            ObjectTypeDef, // ordinal: 93
        
            PropertyTypeDef, // ordinal: 94
        
            TupleTypeDef, // ordinal: 95
        
            TypeParameterNode, // ordinal: 96
        
            TypeAliasDeclaration, // ordinal: 97
        
            FunctionTypeDef, // ordinal: 98
        
            FunctionArgTypeDef, // ordinal: 99
        
            EnumDeclaration, // ordinal: 100
        
            EnumMember, // ordinal: 101
        
            ClassDefinition, // ordinal: 102
        
            FieldDeclaration, // ordinal: 103
        
            CustomExpression, // ordinal: 104
        
}
