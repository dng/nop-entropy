package io.nop.rule.dao;

public interface _NopRuleDaoConstants {
    
    /**
     * 规则类型: 决策树 
     */
    String RULE_TYPE_TREE = "TREE";
                    
    /**
     * 规则类型: 决策表 
     */
    String RULE_TYPE_TABL = "TABL";
                    
}
